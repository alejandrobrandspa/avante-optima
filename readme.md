## OPTIMA

### Backend
- laravel 4.2
- mysql

### Frontend
- react
- redux
- redux-thunk
- axios
- babel/es6

### Tests
- jest
- enzyme
- moxios

### CI
- semaphore

## tests status
[![Build Status](https://semaphoreci.com/api/v1/alebrandspa/avante-optima/branches/master/badge.svg)](https://semaphoreci.com/alebrandspa/avante-optima)